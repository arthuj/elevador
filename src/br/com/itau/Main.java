package br.com.itau;

public class Main {

    public static void main(String[] args) {
        Elevador elevador = new Elevador();
        elevador.Inicializar(10,16,9);
        elevador.Sobe();
        elevador.ExibirAndar();
        elevador.Descer();
        elevador.ExibirAndar();
        elevador.Entra();
        elevador.Entra();
        elevador.Sai();

    }
}
