package br.com.itau;

public class Elevador {

    private int andarAtual;
    private int totalAndar;
    private  int capacidade;
    private  int qtdePessoas;

    public Elevador() {
    }

    public void  Inicializar(int capacidade, int total, int qtdePessoas)
    {
        setAndarAtual(0);
        setCapacidade(capacidade);
        setTotalAndar(total);
        setQtdePessoas(qtdePessoas);
    }

    public  void Entra()
    {
        if(this.qtdePessoas++ > capacidade)
            System.out.println("O elevador está na capacidade máxima");
        else
            qtdePessoas++;
    }

    public  void Sai()
    {
        if(this.qtdePessoas > 0)
            qtdePessoas--;
        else
            System.out.println("Não tem pessoas no elevador");
    }

    public  void  Sobe()
    {
        if(this.andarAtual < totalAndar)
            andarAtual++;
        else
            System.out.println("O elevador está no último andar");
    }

    public  void Descer()
    {
        if(this.andarAtual > 0)
            andarAtual--;
        else
            System.out.println("O elevador está no térreo");
    }

    public  void ExibirAndar()
    {
        System.out.println(this.andarAtual);
    }

    public int getAndarAtual() {
        return andarAtual;
    }

    public void setAndarAtual(int andarAtual) {
        this.andarAtual = andarAtual;
    }

    public int getTotalAndar() {
        return totalAndar;
    }

    public void setTotalAndar(int totalAndar) {
        this.totalAndar = totalAndar;
    }

    public int getCapacidade() {
        return capacidade;
    }

    public void setCapacidade(int capacidade) {
        this.capacidade = capacidade;
    }

    public int getQtdePessoas() {
        return qtdePessoas;
    }

    public void setQtdePessoas(int qtdePessoas) {
        this.qtdePessoas = qtdePessoas;
    }
}
